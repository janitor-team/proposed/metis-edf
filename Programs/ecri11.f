      subroutine ecri11(invp,perm,supnd,parent,n,nbsn,opc,maxlnz,lgind)
      implicit none
      integer invp(*), perm(*),supnd(*),parent(*),n,nbsn,i
      integer maxlnz,lgind
      real*8 opc
      open(FILE='fort.85',UNIT=85,FORM='FORMATTED')
      rewind 85
      print *,' METIS_ecri11 ', nbsn,opc,maxlnz,lgind
      write(85,1001) nbsn,opc,maxlnz,lgind
 1001 FORMAT(I10,E20.6,2I10)
      write(85,1000) (invp(i+1),i=1,n)
      write(85,1000) (perm(i+1),i=1,n)
      write(85,1000) (supnd(i+1),i=1,nbsn+1)
      write(85,1000) (parent(i+1),i=1,nbsn)
 1000 FORMAT(8I10)
      close(UNIT=85)
      end
