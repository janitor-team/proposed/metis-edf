 /* 
  * Copyright 1997, Regents of the University of Minnesota 
  * 
  * frename.c 
  *  
  * This file contains some renaming routines to deal with different Fortran compilers 
  * 
  * Started 9/15/97 
  * George 
  * 
  * $Id: frename.c,v 1.1 1998/11/27 17:59:14 karypis Exp $ 
  * 
  */ 

 #include <metis.h> 


 void METIS_PARTGRAPHRECURSIVE(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_PartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_partgraphrecursive(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 {  
   METIS_PartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part);  
 } 
 void metis_partgraphrecursive_(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_PartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_partgraphrecursive__(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_PartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 


 void METIS_WPARTGRAPHRECURSIVE(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 
 void metis_wpartgraphrecursive(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 
 void metis_wpartgraphrecursive_(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 
 void metis_wpartgraphrecursive__(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphRecursive(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 



 void METIS_PARTGRAPHKWAY(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_PartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_partgraphkway(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_PartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_partgraphkway_(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_PartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_partgraphkway__(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_PartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 



 void METIS_WPARTGRAPHKWAY(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 
 void metis_wpartgraphkway(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 
 void metis_wpartgraphkway_(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 
 void metis_wpartgraphkway__(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_WPartGraphKway(nvtxs, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, tpwgts, options, edgecut, part); 
 } 



 void METIS_EDGEND(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_EdgeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 
 void metis_edgend(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_EdgeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 
 void metis_edgend_(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_EdgeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 
 void metis_edgend__(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_EdgeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 



 void METIS_NODEND(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 
 void metis_nodend(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 
 void metis_nodend_(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 
 void metis_nodend__(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeND(nvtxs, xadj, adjncy, numflag, options, perm, iperm); 
 } 



 void METIS_NODEWND(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeWND(nvtxs, xadj, adjncy, vwgt, numflag, options, perm, iperm); 
 } 
 void metis_nodewnd(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeWND(nvtxs, xadj, adjncy, vwgt, numflag, options, perm, iperm); 
 } 
 void metis_nodewnd_(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeWND(nvtxs, xadj, adjncy, vwgt, numflag, options, perm, iperm); 
 } 
 void metis_nodewnd__(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, long *numflag, long *options, idxtype *perm, idxtype *iperm) 
 { 
   METIS_NodeWND(nvtxs, xadj, adjncy, vwgt, numflag, options, perm, iperm); 
 } 



 void METIS_PARTMESHNODAL(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshNodal(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 
 void metis_partmeshnodal(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshNodal(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 
 void metis_partmeshnodal_(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshNodal(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 
 void metis_partmeshnodal__(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshNodal(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 


 void METIS_PARTMESHDUAL(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshDual(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 
 void metis_partmeshdual(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshDual(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 
 void metis_partmeshdual_(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshDual(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 
 void metis_partmeshdual__(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, long *nparts, long *edgecut, idxtype *epart, idxtype *npart) 
 { 
   METIS_PartMeshDual(ne, nn, elmnts, etype, numflag, nparts, edgecut, epart, npart); 
 } 


 void METIS_MESHTONODAL(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToNodal(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 
 void metis_meshtonodal(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToNodal(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 
 void metis_meshtonodal_(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToNodal(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 
 void metis_meshtonodal__(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToNodal(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 


 void METIS_MESHTODUAL(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToDual(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 
 void metis_meshtodual(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToDual(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 
 void metis_meshtodual_(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToDual(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 
 void metis_meshtodual__(long *ne, long *nn, idxtype *elmnts, long *etype, long *numflag, idxtype *dxadj, idxtype *dadjncy) 
 { 
   METIS_MeshToDual(ne, nn, elmnts, etype, numflag, dxadj, dadjncy); 
 } 


 void METIS_ESTIMATEMEMORY(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *optype, long *nbytes) 
 { 
   METIS_EstimateMemory(nvtxs, xadj, adjncy, numflag, optype, nbytes); 
 } 
 void metis_estimatememory(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *optype, long *nbytes) 
 { 
   METIS_EstimateMemory(nvtxs, xadj, adjncy, numflag, optype, nbytes); 
 } 
 void metis_estimatememory_(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *optype, long *nbytes) 
 { 
   METIS_EstimateMemory(nvtxs, xadj, adjncy, numflag, optype, nbytes); 
 } 
 void metis_estimatememory__(long *nvtxs, idxtype *xadj, idxtype *adjncy, long *numflag, long *optype, long *nbytes) 
 { 
   METIS_EstimateMemory(nvtxs, xadj, adjncy, numflag, optype, nbytes); 
 } 



 void METIS_MCPARTGRAPHRECURSIVE(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_mcpartgraphrecursive(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_mcpartgraphrecursive_(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 
 void metis_mcpartgraphrecursive__(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part); 
 } 


 void METIS_MCPARTGRAPHKWAY(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *rubvec, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, rubvec, options, edgecut, part); 
 } 
 void metis_mcpartgraphkway(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *rubvec, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, rubvec, options, edgecut, part); 
 } 
 void metis_mcpartgraphkway_(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *rubvec, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, rubvec, options, edgecut, part); 
 } 
 void metis_mcpartgraphkway__(long *nvtxs, long *ncon, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *adjwgt, long *wgtflag, long *numflag, long *nparts, float *rubvec, long *options, long *edgecut, idxtype *part) 
 { 
   METIS_mCPartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, rubvec, options, edgecut, part); 
 } 


 void METIS_PARTGRAPHVKWAY(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, long *options, long *volume, idxtype *part) 
 { 
   METIS_PartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, options, volume, part); 
 } 
 void metis_partgraphvkway(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, long *options, long *volume, idxtype *part) 
 { 
   METIS_PartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, options, volume, part); 
 } 
 void metis_partgraphvkway_(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, long *options, long *volume, idxtype *part) 
 { 
   METIS_PartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, options, volume, part); 
 } 
 void metis_partgraphvkway__(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, long *options, long *volume, idxtype *part) 
 { 
   METIS_PartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, options, volume, part); 
 } 

 void METIS_WPARTGRAPHVKWAY(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *volume, idxtype *part) 
 { 
   METIS_WPartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, tpwgts, options, volume, part); 
 } 
 void metis_wpartgraphvkway(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *volume, idxtype *part) 
 { 
   METIS_WPartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, tpwgts, options, volume, part); 
 } 
 void metis_wpartgraphvkway_(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *volume, idxtype *part) 
 { 
   METIS_WPartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, tpwgts, options, volume, part); 
 } 
 void metis_wpartgraphvkway__(long *nvtxs, idxtype *xadj, idxtype *adjncy, idxtype *vwgt, idxtype *vsize, long *wgtflag, long *numflag, long *nparts, float *tpwgts, long *options, long *volume, idxtype *part) 
 { 
   METIS_WPartGraphVKway(nvtxs, xadj, adjncy, vwgt, vsize, wgtflag, numflag, nparts, tpwgts, options, volume, part); 
 } 



